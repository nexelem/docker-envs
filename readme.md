```
________                 __                                                    
\______ \   ____   ____ |  | __ ___________            ____   _______  ________
 |    |  \ /  _ \_/ ___\|  |/ // __ \_  __ \  ______ _/ __ \ /    \  \/ /  ___/
 |    |   (  <_> )  \___|    <\  ___/|  | \/ /_____/ \  ___/|   |  \   /\___ \ 
/_______  /\____/ \___  >__|_ \\___  >__|             \___  >___|  /\_//____  >
        \/            \/     \/    \/                     \/     \/         \/ 
```
# Description

This repo contains colleciton of initial Docker scripts to create various images. This is to be used as base for custom environments for particular projects.

# Prerequisites for development

We are following classic gitflow in this project [Gitflow branching model](http://nvie.com/posts/a-successful-git-branching-model/) which means we do not fork anymore.

Install gitflow workflow: [Gitflow installation](https://github.com/nvie/gitflow/wiki/Installation)
More about how gitflow helps with this workflow (commands) can be found on Giflow repo: [Gitflow commands](https://github.com/nvie/gitflow) (README)

We start with default initialization: `gitflow init -d`

To make work more fluent you may want to install gitflow completion: [Gitflow completion](https://github.com/bobthecow/git-flow-completion)

# General dev environment setup with Docker

- Download and install [Docker](https://docs.docker.com/installation/) for your operating system. Make sure `docker` is on your path (on *nixes done automatically)
- Make sure Docker service is started:

```
$ sudo systemctl start docker
```

If you want Docker to start at boot, you should also:

```
$ sudo systemctl enable docker
```

- Make sure you user is added to docker group e.g. `sudo usermod -a -G docker <<user>>`
- In case you want to force rebuild machine use run `./rebuild.sh` script. This also sets up your environment at the beginning.
- For running environment use `run.sh` script
- In case of Windows use: [Windows instructions](http://docs.docker.com/installation/windows/)


