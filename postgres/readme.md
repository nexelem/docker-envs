# Description
This directory contains Dockerfile script for creating simple image with preinstalled Postgres 9.3 database.

# Accessing postgres
Postgres endpoint is reachable on your host under localhost:5432 - which is default PostgreSQL port. Name of the database is `test_db`, user: `db_user`, password: `db_user`.
As for the client you may use e.g. [pgAdmin3](http://www.pgadmin.org/)